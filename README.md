# astronvim導入メモ

AUTHOR: M.TANAKA   
CREATE: 2023/09/25  
UPDATE: 2023/09/25  

[TOC]

## 前提

環境は，
- Windows 11
- Kubuntu 22.04
の２つで試した．

## メモ

### 1. インストール

[公式サイト](https://astronvim.com/)の通りに実施．
- Nerd Fontはたくさん種類があるが，Nerd Hack Fontを選択．
    - インストール方法は，Linuxの場合[ここ](https://linuxspin.com/install-nerd-fonts-on-ubuntu/)を参考にした．
    - neovimは各種ターミナルで開くので，Windows Terminal/Konsoleのデフォルトフォントをこれに変更しておく．
- neovimのインストールは，[公式サイト](https://github.com/neovim/neovim/wiki/Installing-Neovim)を参照．aptではバージョンが古いのでsnapを使った．

    ```
    winget install Neovim.Neovim
    ```

    ```
    sudo snap install --beta nvim --classic
    ```

### 2. 言語サーバやらなんやら

正直良くわかっていない．Winodwsの場合，公式の通りにコマンドを実行すると，下記のエラーが出る．

```vim
:LspInstall pyright
Failed
    ✗ pyright
      ▶ # [2/2] spawn: npm.cmd failed with exit code - and signal -. npm.cmd is not executable
```

```vim
:TSInstall python 
No C compiler found! "cc", "gcc", "clang", "cl", "zig" are not executable.
```

```vim:
DapInstall python
Error executing vim.schedule lua callback: ...-dap.nvim/lua/mason-nvim-dap/mappings/configurations.lua:99: module 'dap.utils' not found:
        no field package.preload['dap.utils']
cache_loader: module dap.utils not found
cache_loader_lib: module dap.utils not found
        no file '.\dap\utils.lua'
        no file 'C:\Program Files\Neovim\bin\lua\dap\utils.lua'
        no file 'C:\Program Files\Neovim\bin\lua\dap\utils\init.lua'
        no file '.\dap\utils.dll'
        no file 'C:\Program Files\Neovim\bin\dap\utils.dll'
        no file 'C:\Program Files\Neovim\bin\loadall.dll'
        no file '.\dap.dll'
        no file 'C:\Program Files\Neovim\bin\dap.dll'
        no file 'C:\Program Files\Neovim\bin\loadall.dll'
stack traceback:
        [C]: in function 'require'
        ...-dap.nvim/lua/mason-nvim-dap/mappings/configurations.lua:99: in main chunk
        [C]: in function 'require'
        ...ata/lazy/mason-nvim-dap.nvim/lua/mason-nvim-dap/init.lua:45: in function 'fn'
        ...al/nvim-data/lazy/mason.nvim/lua/mason-core/optional.lua:101: in function 'if_present'
        ...ata/lazy/mason-nvim-dap.nvim/lua/mason-nvim-dap/init.lua:41: in function 'fn'
        ...al/nvim-data/lazy/mason.nvim/lua/mason-core/optional.lua:101: in function 'if_present'
        ...ata/lazy/mason-nvim-dap.nvim/lua/mason-nvim-dap/init.lua:65: in function 'cb'
        vim/_editor.lua:263: in function <vim/_editor.lua:262>
```

Linuxの場合も類似のエラーが出たが，２，３個目のエラーはPC再起動で直り，１つめのエラーは`sudo apt install npm`で直った．

npmというパッケージがないことが原因？

clangをインストールしてみる．

```
> winget search clang
名前               ID                バージョン 一致           ソース
---------------------------------------------------------------------
LLVM               LLVM.LLVM         17.0.1     Command: clang winget
CrossWorks for ARM Rowley.CrossWorks 4.8        Tag: clang     winget
clangd             LLVM.clangd       16.0.2                    winget

> winget install LLVM.LLVM
```

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/create-environment/astronvim.git
git branch -M main
git push -uf origin main
```

https://linuxspin.com/install-nerd-fonts-on-ubuntu/